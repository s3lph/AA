.. Administration Association documentation master file, created by
   sphinx-quickstart on Wed Apr 19 21:41:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Willkommen zur Dokumentation der Administration Association
***********************************************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Abläufe des CCC Office
======================

Das CCC Office hat eine Sammlung routinierter Abläufe zur Bearbeitung aller Anliegen. Diese wurden in dieser Software
abgebildet und sind hier thematisch sortiert dokumentiert. Diese Tätigkeiten und Workflows umfassen:

* E-Mails lesen und bearbeiten (mehrmals die Woche)
* Mitgliederanträge und Mitgliedsänderungen bearbeiten (mehrmals die Woche)
* Kontoauszüge abholen und Beitragszahlungen in die Datenbank einpflegen (min. 2 mal pro Monat)
* Erfastatistik erstellen und versenden (Am Ende eines jeden Monats)
* Zahlungserinnerungsmails verschicken (1 mal im Quartal per E-Mail, 1 mal im Jahr per Snail-Mail)
* Mitgliederlisten-Export für Events erstellen (Zu den jeweiligen Events)
* Datenschleuder-Etiketten drucken (Wenn die DS Redaktion danach fragt)

Geldangelegenheiten
-------------------

Kontoauszüge
++++++++++++

Es wird derzeit nur das CSV-Format der Postbank-Kontoauszüge verarbeitet. Die Dateien heißen
PB_Umsatzauskunft_KtoNr<kontonummer>_<datum>.csv und können über Member Interaction > Banking > Import banking csv oder
direkt unter dem Link /import_app/import_banking_csv/ hochgeladen werden. Überlappungen des Datumbereichs mit vorherigen
Importen werden automatisch erkannt und stellen kein Problem dar.

Ausgehende Zahlungen werden ignoriert. Eingehende Zahlungen werden auf Chaosnummern untersucht und versucht mit
Mitgliedern zu matchen. Sollte dies nicht funktionieren, kann eine Chaosnummer in der linken Spalte angegeben werden.
Es ist möglich parallel in einem weiteren Fenster nach Mitgliedern zu suchen.

Die eingegebenen CSV-Dateien müssen nicht archiviert werden.

Umgang mit Barzahlungen
+++++++++++++++++++++++

Wenn ein Mitglied bar bezahlt, dann muss die Account balance im Administrations-Interface direkt editiert werden.
Dieses Interface erreicht man indem man in der Suche bei einem gefundenen Mitglied auf die Chaosnummer klickt oder wenn
man nach /admin/members/member geht und dort ein Mitglied auswählt.

Zahlungserinnerungen
++++++++++++++++++++

Mitglieder haben zwei Felder, die für die Mitgliedsbeiträge relevant sind. Das Feld Fee Paid Until gibt an bis wann der
aktuelle Mitgliedsbeitrag gültig ist. Das Feld Account Balance gibt an wie viel Guthaben das Mitglied noch hat. Wenn
das Datem des Feldes Fee Paid Until erreicht wurde, dann wird im nächsten Zahlungsdurchlauf geprüft, ob genügend Geld
auf der Account Balance zur Verfügung steht um den nächsten Mitgliedsbeitrag zu bezahlen.

Zahlungsläufe sollten monatlich durchgeführt werden. Dazu geht man unter Member Interaction > Payment/Billing >
execute billing cycle. Anschließend kann man mit dem Menüpunkt create delayed payment mails die Generierung der
Zahlungsverzugserinnerungs-Mails veranlassen. Diese werden dann in die Sende-Queue gelegt und müssen explizit versandt
werden.

Verwaltung der Mitglieder
-------------------------

Eintritt eines Mitglieds
++++++++++++++++++++++++

Auf der Startseite verbirgt sich hinter dem Button "Add Member" das Eingabeformular. Vor der Eingabe eines neuen
Mitglieds sollte per Suche überprüft werden, ob es sich nicht um einen doppelt gestellten Mitgliedsantrag handelt,
denn das wird noch nicht automatisch bei der Eingabe gemacht.

Die Adresse wird in die drei Felder "Address 1", "Address 2" und "Address 3" eingegeben. Freiform-Felder erlauben es
hier jede Art von Adresse in jeder erdenklichen Form einzugeben. Das Land wird in dem Feld darunter ausgewählt.

Es können beliebig viele Email-Adressen angegeben werden und jeweils ein GPG-Key dazu. In das Feld kann der gesamte
Fingerprint, die Long- oder die Short-ID eingetragen werden. Der Key muss sich auf einem Keyserver befinden und wird
beim Versand heruntergeladen. Das Speichern ganzer Keys in der Software wird nicht unterstützt.

Datenschleuder-Abonnenten können nur im Admin-Interface eingegeben werden.

Rückläufer wegen falscher Adresse
+++++++++++++++++++++++++++++++++

Das Mitglied muss gesucht werden und im Administrations-Interface editiert werden. Dort muss dann das Häkchen bei
"Address unknown" gesetzt werden. Es wird automatisch eine entsprechende Email generiert und in die Sende-Queue gelegt.

Austritt eines Mitglieds
++++++++++++++++++++++++

In den Suchergebnissen gibt es in der rechten Spalte ein Stoppschildsymbol. Ein Klick darauf löscht das Mitglied ohne
Nachfrage und unwiderruflich aus der Datenbank. Vorher wird noch eine Email mit einer Austrittsbestätigung an das
gesandt.

Derzeit ist es noch nicht möglich einfach das Austrittsdatum des Mitglieds anzugeben um es automatisch zum gewünschten
Zeitpunkt austreten zu lassen.

Versand von Mails
-----------------

Mails werden an verschiedenen Stellen im System automatisch generiert und in eine Queue gelegt. Diese Queue kann unter
Member Interaction > Mails > Emails to Send oder direkt unter /admin/members/emailtomember/ eingesehen werden. Es ist
möglich Emails aus der Queue zu löschen, zu editieren und auch anzulegen.

.. note:: Wenn es nur darum geht den aktuellen Datensatz einem Mitglied zukommen zu lassen, dann geht das komfortabler
          über die Suchergebnisse. Einfach das Briefsymbol neben dem gefundenen Mitglied anklicken.

Um die Emails zu versenden muss man direkt die URL /api/mail_send_all/ aufrufen um alle Emails in der Queue zu senden
oder /api/mail_send_next/ um die nächste Mail in der Queue zu senden.

Vereinstisch/Kassen-Ex/Import
-----------------------------

Unter Member Interaction > Export/Import > Cashpoint Export gibt es die Möglichkeit die Datenbank als
Tabulator-separierte .csv herunterzuladen. Die Daten sind für die Kasse des Congress und Camps geeignet, aber nicht
für den Vereinstisch.

Import für dieses Format existiert noch nicht.

Erfa-Statistik
--------------

Einmal im Monat sollte die Statistik für den Vorstand per Email versandt werden. Dies geht durch Aufruf der URL
/api/get_erfa_statistics/

Indizes und Tabellen
====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
