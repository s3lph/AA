#!/bin/bash -x
PORT=8000

#python3 -m venv --system-site-packages env
#source env/bin/activate
#pip install --upgrade wheel
#pip install --upgrade -r requirements.txt
#python3 ROOT/manage.py migrate
python3 ROOT/manage.py runserver 127.0.0.1:$PORT
