from hypothesis.extra.django import TestCase, from_model
from hypothesis.strategies import integers
from hypothesis import given

from members.models import DeliveryNumber, Person

delivery_num_strategy = from_model(
    DeliveryNumber, recipient=from_model(Person, chaos_number=integers(min_value=1, max_value=100000000)))


class DeliveryNumberTestCase(TestCase):
    @given(delivery_num_strategy)
    def test_create_delivery_numbers(self, del_num):
        assert len(del_num.number) == 8

        assert del_num.number[0] == 'D'
        for i in range(1, 8):
            assert del_num.number[i] in 'ABCDEFGHJKLMNPQRSTUVWXYZ3789'

        assert not del_num.returned
