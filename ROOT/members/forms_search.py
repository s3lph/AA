from django import forms


class SearchForm(forms.Form):
    # Default search is AND
    # Optional OR
    # queryset.filter(field_name__icontains=bla)

    # Search Fields
    member_first_name = forms.CharField(required=False)
    member_last_name = forms.CharField(required=False)
    member_chaos_id = forms.CharField(required=False)
    member_address = forms.CharField(required=False)
    member_email_address = forms.CharField(required=False)

    # Filter Fields
    member_fee_reduced = forms.BooleanField(required=False)
    is_active = forms.BooleanField(required=False)
    apply_filters = forms.BooleanField(required=False)

    check_empty = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                            choices=(("first", "First name"),
                                                     ("last", "Last name"),
                                                     ("address", "Address"),
                                                     ("country", "Country"),
                                                     ("email", "Email address")))
    clear_results_before_query = forms.BooleanField(initial=True)
