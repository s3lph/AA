import gpg
import members.models as mm
import other.gpg_util as gu
import other.test_gpg as test_gpg

from django.conf import settings
from django.test import TestCase, override_settings


def mock_member(keyid, keyid2=''):
    member = mm.Member.objects.create(
        chaos_number=666,
        first_name='Jane',
        last_name='Doe',
        account_balance=60,
        membership_type=mm.Member.MEMBERSHIP_TYPE_MEMBER,
    )
    member.save()
    # Note: if the primary address has no key but the secondary does,
    # The key in the secondary address is never checked.
    # Seems like an unusual thing to happen, but I'm not sure if it's
    # wise to have logic depend on unenforced constraints in the data ...
    email_address_1 = mm.EmailAddress(person=member,
                                      email_address='a@example.com',
                                      gpg_key_id=keyid2,
                                      is_primary=False)
    email_address_2 = mm.EmailAddress(person=member,
                                      email_address='b@example.com',
                                      gpg_key_id=keyid,
                                      is_primary=True)
    email_address_1.save()
    email_address_2.save()
    return member


def send_stub(addresses, subject, body, attachments=[], should_archive=True):
    return (addresses, subject, body, attachments)


def stubbed_email_to(member, tpe=mm.EmailToMember.SEND_TYPE_DATA_RECORD):
    mail = mm.EmailToMember()
    mail.member = member
    mail._send_mail = send_stub
    mail.email_type = tpe
    mail.render_subject_and_body()
    return mail


@override_settings(GPG_HOST_USER=test_gpg.SE_KEYID)
class MemberEmailTestCase(TestCase):
    def setUp(self):
        ctx = gpg.Context(armor=True)
        ctx.set_engine_info(gpg.constants.protocol.OpenPGP,
                            home_dir=settings.GPG_HOME)
        ctx.set_keylist_mode(gpg.constants.keylist.mode.LOCAL)
        for keyid in test_gpg.ALL_KEYIDS:
            try:
                key = ctx.get_key(keyid)
                gpg._gpgme.gpgme_op_delete(ctx.wrapped, key,
                                           gpg._gpgme.GPGME_DELETE_FORCE)
            except Exception:
                # Errors don't matter, we just need a clean keyring
                # Most of the keys will not even be in the keyring at setup
                pass

    def test_valid_key(self):
        m = mock_member(test_gpg.SE_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        self.assertEqual(
            test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)

    def test_valid_and_invalid_key(self):
        m = mock_member(test_gpg.INVALID_KEYID, test_gpg.SE_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 2)
        addresses, subject, body, attachments = mails_sent[1]
        self.assertEqual(addresses[0], m.get_emails()[1].email_address)
        self.assertEqual(
            test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)

        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_INVALID)
        self.assertIn(gu.ERRMSG_INVALID, body)

    def test_key_without_UIDs(self):
        with self.settings(GPG_KEYSERVERS=['hkps://keys.openpgp.org']):
            m = mock_member('D04929749A386D57107005A348F437EC4CC6878E')
            mail = stubbed_email_to(m)
            mails_sent = mail.send(True)
            self.assertEqual(len(mails_sent), 1)
            addresses, subject, body, attachments = mails_sent[0]
            self.assertEqual(addresses[0], m.get_emails()[0].email_address)
            test_gpg.verify(body.encode())
            with self.assertRaises(gpg.errors.GPGMEError):
                self.assertEqual(
                    test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
            self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_NOTIMPORTED)
            self.assertIn(gu.ERRMSG_NOTIMPORTED, body)

    def test_valid_and_revoked_key(self):
        m = mock_member(test_gpg.REVOKED_KEYID, test_gpg.SE_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 2)
        addresses, subject, body, attachments = mails_sent[1]
        self.assertEqual(addresses[0], m.get_emails()[1].email_address)
        self.assertEqual(
            test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)

        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_REVOKED)
        self.assertIn(gu.ERRMSG_REVOKED, body)

    def test_valid_and_expired_key(self):
        m = mock_member(test_gpg.EXPIRED_KEYID, test_gpg.SE_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 2)
        addresses, subject, body, attachments = mails_sent[1]
        self.assertEqual(addresses[0], m.get_emails()[1].email_address)
        self.assertEqual(
            test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)

        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_EXPIRED)
        self.assertIn(gu.ERRMSG_EXPIRED, body)

    def test_invalid_key(self):
        m = mock_member(test_gpg.INVALID_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        test_gpg.verify(body.encode())
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_INVALID)
        self.assertIn(gu.ERRMSG_INVALID, body)

    def test_evil32_key(self):
        m = mock_member(test_gpg.EVIL32_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        test_gpg.verify(body.encode())
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_AMBIGUOUS)
        self.assertIn(gu.ERRMSG_AMBIGUOUS, body)

    def test_revoked_key(self):
        m = mock_member(test_gpg.REVOKED_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        test_gpg.verify(body.encode())
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_REVOKED)
        self.assertIn(gu.ERRMSG_REVOKED, body)

    def test_expired_key(self):
        m = mock_member(test_gpg.EXPIRED_KEYID)
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        test_gpg.verify(body.encode())
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)
        self.assertEqual(m.get_emails()[0].gpg_error, gu.ERRMSG_EXPIRED)
        self.assertIn(gu.ERRMSG_EXPIRED, body)

    def test_no_key(self):
        m = mock_member('')
        mail = stubbed_email_to(m)
        mails_sent = mail.send(True)
        self.assertEqual(len(mails_sent), 1)
        addresses, subject, body, attachments = mails_sent[0]
        self.assertEqual(addresses[0], m.get_emails()[0].email_address)
        test_gpg.verify(body.encode())
        with self.assertRaises(gpg.errors.GPGMEError):
            self.assertEqual(
                test_gpg.decrypt(body.encode()).decode('utf-8'), mail.body)

    @override_settings(GPG_KEYSERVERS=[test_gpg.UNREACHABLE_KEYSERVER])
    def test_no_mail_if_servers_unreachable(self):
        m = mock_member(test_gpg.VALID_KEYID)
        mail = stubbed_email_to(m)
        resultset = mail.send(True)
        self.assertEqual(resultset, [])

    def test_validate_api_compat1(self):
        # One valid key
        m = mock_member(test_gpg.VALID_KEYID)
        mail = stubbed_email_to(m)
        resultset = mail.send()
        self.assertEqual(len(resultset), 4)

    def test_validate_api_compat2(self):
        # One expired, one valid key --> Two Mails sent, but only one returned, because reasons.
        m = mock_member(test_gpg.EXPIRED_KEYID, test_gpg.SE_KEYID)
        mail = stubbed_email_to(m)
        resultset = mail.send()
        self.assertEqual(len(resultset), 4)

    def test_validate_api_compat3(self):
        # One invalid key --> One Mail sent (error message)
        m = mock_member(test_gpg.INVALID_KEYID)
        mail = stubbed_email_to(m)
        resultset = mail.send()
        self.assertEqual(len(resultset), 4)

    @override_settings(GPG_KEYSERVERS=[test_gpg.UNREACHABLE_KEYSERVER])
    def test_validate_api_compat_unreachable(self):
        # One valid key
        m = mock_member(test_gpg.VALID_KEYID)
        mail = stubbed_email_to(m)
        result = mail.send()
        self.assertEqual(result, {"mailTo": [], "send_state": 0})
