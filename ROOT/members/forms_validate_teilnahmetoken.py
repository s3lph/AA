from django import forms
from django.core.validators import RegexValidator
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

teilnahme_token_validator = RegexValidator(r"[a-zA-Z]{10}|[0-9]{1,5}",
                                           "Token muss aus exakt 10 Buchstaben bestehen oder Chaosnummer mit 1 bis 5 Stellen")


class TeilnahmeValidationForm(forms.Form):
    teilnahme_token = forms.CharField(max_length=10, validators=[teilnahme_token_validator],
                                      label='Identifikationstoken oder Chaosnummer', required=False,
                                      widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))

    def __init__(self, *args, **kwargs):
        super(TeilnahmeValidationForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-9'
        self.helper.add_input(Submit('submit', 'Submit'))
