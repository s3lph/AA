from django import forms
from members.models import PremiumAddressLabel
import datetime


def in_two_weeks():
    return datetime.date.today() + datetime.timedelta(weeks=2)


class DSAddressLabelForm(forms.Form):
    pa_id = forms.ModelChoiceField(queryset=PremiumAddressLabel.objects.all(), empty_label='None',
                                   label='PremiumAddress-Label', required=False)
    num_pages = forms.IntegerField(min_value=0, label='Number of pages of this issue')
    pickup_date = forms.DateField(initial=in_two_weeks, label='Date of the planned pick-up through the postal service')
