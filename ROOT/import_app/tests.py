# -*- coding: utf-8 -*-

import csv
import logging
import unittest
from io import StringIO
from datetime import datetime

import django
from django.test import TestCase

from .csv_parser import TransactionReader
from .models import Transaction
from members.models import Member

django.setup()

logging.getLogger(__name__)
logging.getLogger().setLevel(logging.DEBUG)

year = datetime.now().year
current_year_str = str(year)
next_year_str = str(year + 1)
funky_year_str = "".join(next_year_str[0:3] + " " + next_year_str[3])

csv_string = StringIO("""31.07.2015;31.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck Chaos-Nr.: 500001;Vorname Name;Chaos Computer Club e.V.;20;123,45
30.07.2015;30.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck CHAOSNUMMER 5004 ;Vorname Name;Chaos Computer Club e.V.;72;483,45
31.07.2015;31.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck ChaosNr: 7001 ;Vorname Name;Chaos Computer Club e.V.;72;339,45
28.07.2015;28.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck 301 Mitgliedsbeitrag ;Vorname Name;Chaos Computer Club e.V.;72;1095,45
29.07.2015;29.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck 15-16 Mitgliedsbeitrag Vorname Nachna me ;ER NACHNAME UND SIE NACHNAME;Chaos Computer Club e.V.;72;699,45
28.07.2015;28.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck Mitgliedsbeitraege: 8001 (""" + current_year_str + """,""" + funky_year_str + """) ;Vorname Name;Chaos Computer Club e.V.;72;879,45
30.07.2015;30.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck mitgliedschaftsbeitrag """ + current_year_str + """+""" + next_year_str + """ f. chaosnr. 5003 ;Vorname Name;Chaos Computer Club e.V.;72;411,45
28.07.2015;28.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck Mitgliedsbeitraege (""" + current_year_str + """,""" + funky_year_str + """ ;Vorname Name;Chaos Computer Club e.V.;72;879,45
28.07.2015;28.07.2015;Gutschrift;Referenz DAB/B3/012345678/2015-07-2 7 Verwendungszweck CHAOS-NR.: 9002, MITGLIEDSBEITRAG 2 015 ;Vorname Name;Chaos Computer Club e.V.;72;951,45
28.07.2015;28.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck Mitgliedsbeitrag 2015 ChaosNummer: 4002 ;VORNAME MITTELNAME NACHNAME;Chaos Computer Club e.V.;72;1023,45
27.07.2015;27.07.2015;Gutschrift;Referenz NOTPROVIDED Verwendungszweck Chaosnr. 6001, BEITRAG 08/2015-08/2 017 ;Nachname, Vorname;Chaos Computer Club e.V.;144;1383,45
24.07.2015;24.07.2015;Gutschrift;Referenz NOTPROVIDED Auftraggeber Identifikation GENODEM1GLS Verwendungszweck Spende Danke fuer eure Arbeit em ail at domain.tld ;Vorname Name;Chaos Computer Club e.V.;10;1537,45
22.07.2015;22.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck CHAOS-NR. """ + next_year_str + """, MITGLIEDSBEITRAG ;Er Nachname und Sie Nachname;Chaos Computer Club e.V.;72;2093,45
22.07.2015;22.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck """ + current_year_str + """; Er Nachname und Sie Nachname;Chaos Computer Club e.V.;72;2093,45
22.07.2015;22.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck """ + current_year_str + """ 3000; Er Nachname und Sie Nachname;Chaos Computer Club e.V.;72;2093,45
22.07.2015;22.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck """ + current_year_str + """ chaosnr 3000; Er Nachname und Sie Nachname;Chaos Computer Club e.V.;72;2093,45"""
)


def line_from_csv(line):
    csvin = csv.reader(csv_string, delimiter=';')
    csvin = list(csvin)
    csv_string.seek(0)
    return csvin[line]


def make_transaction_reader(line):
    line_split = line_from_csv(line)
    tr = TransactionReader(line_split)
    return tr


def make_transaction(line):
    tr = make_transaction_reader(line)
    transaction = Transaction()
    transaction.add_transaction(tr)
    return transaction


class TransactionReaderTest(TestCase):
    def setUp(self):
        for number in [year, 3000, year + 1, 8001, 5003, int(next_year_str[3]), int(next_year_str[0:3]), 15, 16]:
            Member.objects.create(chaos_number=number,
                                  first_name='foo',
                                  last_name='bar',
                                  address_1='street',
                                  address_country='DE')

    def test_chaos_nr_regex(self):  # these should match on regex(s) for chaos number/mitgliedsbeitrag w/ high confidence
        for i in range(3):
            tr = make_transaction_reader(i)
            self.assertLess(tr.rating, 8)

    def test_number_mashing(self):
        transaction = make_transaction(4)
        self.assertIn(transaction.member.chaos_number, [15, 16])
        self.assertEqual(transaction.STATUS_UNKNOWN_CHAOS_NR,
                         transaction.status)

    def test_year_split_year5(self):  # wz: Mitgliedsbeitraege: 8001 (2014, 201 5)
        transaction = make_transaction(5)
        self.assertEqual(8001, transaction.member.chaos_number)
        self.assertEqual(Transaction.STATUS_MATCHED_CHAOS_NR,
                         transaction.status)

    def test_year_split_year6(self):  # mitgliedschaftsbeitrag 2014+2015 f. chaosnr. 5003
        transaction = make_transaction(6)
        self.assertEqual(5003, transaction.member.chaos_number)
        self.assertEqual(Transaction.STATUS_COMPLETED, transaction.status)

    @unittest.skip('Test fails if 2014/2015 is replaced by 2020/2022. The tuple [202, 2] receives a rating of 8.')
    def test_year_split_year7(self):  # Mitgliedsbeitraege (2014, 201 5)
        transaction = make_transaction(7)
        self.assertIn(transaction.member.chaos_number, [int(next_year_str[3]), int(next_year_str[0:3])])
        self.assertEqual(Transaction.STATUS_UNKNOWN_CHAOS_NR,
                         transaction.status)

    def test_donation(self):
        transaction = make_transaction(11)
        self.assertEqual(Transaction.STATUS_IGNOREFOREVER, transaction.status)

    @unittest.skip('Test needs to be rewritten, because it depends on running before the year 2020')
    def test_year_after_chaos_key(self):
        transaction = make_transaction(12)
        self.assertEqual(transaction.STATUS_UNKNOWN_CHAOS_NR,
                         transaction.status)  # erring with no false positives
        self.assertEqual(year + 1, transaction.member.chaos_number)

    def test_skip_year(self):
        transaction = make_transaction(13)  # corresponds to line number in csv, add lines only to end of csv!!
        self.assertEqual(Transaction.STATUS_UNKNOWN_CHAOS_NR,
                         transaction.status)
        self.assertEqual(year, transaction.member.chaos_number)

    def test_multi_numbers(self):
        transaction = make_transaction(14)  # Verwendungszweck 2014 3000 ..
        self.assertEqual(Transaction.STATUS_MATCHED_CHAOS_NR,
                         transaction.status)
        self.assertEqual(3000, transaction.member.chaos_number)

    def test_multi_numbers_chaosnr(self):
        transaction = make_transaction(15)  # Verwendungszweck 2014 chaosnr 3000 ..
        self.assertEqual(Transaction.STATUS_COMPLETED, transaction.status)
        self.assertEqual(3000, transaction.member.chaos_number)


# class TransferTokenTest(TestCase):
#     num_members = 20000
#
#     def setUp(self):
#         for number in range(1, self.num_members+1):
#             Member.objects.create(chaos_number=number, first_name='foo', last_name='bar', address_1='street',
#                                   address_country='DE')
#
#     def test_token_extraction(self):
#         m = Member.objects.order_by('?').first()
#         tr = TransactionReader(
#             "31.07.2015;31.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck Chaos-Nr.: {};\
#  Vorname Name;Chaos Computer Club e.V.;20;123,45".format(m.transfer_token).split(';'))
#         transaction = Transaction()
#         transaction.add_transaction(tr)
#         self.assertEqual(Transaction.STATUS_COMPLETED, transaction.status)
#
#     def test_token_extraction_with_dashes(self):
#         m = Member.objects.order_by('?').first()
#         tr = TransactionReader(
#             "31.07.2015;31.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck Chaos-Nr.: {};\
#  Vorname Name;Chaos Computer Club e.V.;20;123,45".format(m.human_readable_transfer_token()).split(';'))
#         transaction = Transaction()
#         transaction.add_transaction(tr)
#         self.assertEqual(Transaction.STATUS_COMPLETED, transaction.status)
#
#     def test_token_extraction_with_one_error(self):
#         m = Member.objects.order_by('?').first()
#         token = m.human_readable_transfer_token()
#         print(token)
#         token = token[:5] + 'U' + token[6:]
#         print(token)
#         tr = TransactionReader(
#             "31.07.2015;31.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck Chaos-Nr.: {};\
#  Vorname Name;Chaos Computer Club e.V.;20;123,45".format(token).split(';'))
#         print('rating: {}'.format(tr.rating))
#         transaction = Transaction()
#         transaction.add_transaction(tr)
#         self.assertEqual(Transaction.STATUS_MATCHED_CHAOS_NR, transaction.status)
#         self.assertEqual(m.chaos_number, transaction.member.chaos_number)
#
#     def test_token_extraction_with_two_errors(self):
#         m = Member.objects.order_by('?').first()
#         token = m.human_readable_transfer_token()
#         print(token)
#         token = token[:5] + 'U' + token[6:]
#         token = token[:8] + 'U' + token[9:]
#         print(token)
#         tr = TransactionReader(
#             "31.07.2015;31.07.2015;Dauergutschrift;Referenz NOTPROVIDED Verwendungszweck Chaos-Nr.: {};\
#  Vorname Name;Chaos Computer Club e.V.;20;123,45".format(token).split(';'))
#         print('rating: {}'.format(tr.rating))
#         transaction = Transaction()
#         transaction.add_transaction(tr)
#         self.assertEqual(Transaction.STATUS_MATCHED_CHAOS_NR, transaction.status)
#         self.assertEqual(m.chaos_number, transaction.member.chaos_number)
