# Administration Association

This app is written by members of the Chaos Computer Club to help its administration, the office, to manage the
club's memberships. It is not meant to be awesome, just better than DBaseII, which it does pretty well.

The main focuses in development are:
* Replace the DBaseII solution used since at least 1995
* Enable working in parallel
* Be easily modifiable and extensible
* Sending *encrypted* mails to members about any changes in their status
* Reading and processing bank statements in CSV format

You are welcome to fork, send pull request and much more, but the goal of this piece of software will remain running the
Chaos Computer Club's memberships.

## Project Webpage

Find the source and downloads at https://gitlab.com/pythonfoo/AA

## Setup

First create a file ```ROOT/ROOT/settings_production.py``` by using ```ROOT/ROOT/settings.py``` as a template. 

### PostgreSQL

Recommended for production setups because it's much faster than SQLite and does support more than one simultaneous access, which helps parallelization a lot.

Install all the following dependencies:

```
sudo apt-get update
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib
```

Copy the PostgreSQL settings from settings.py to settings_production.py and uncomment them:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'AA',
    }
}
```

Create a user and database:
```
sudo  -u postgres -i
$ createuser <yourusername>
$ createdb AA -O <yourusername>
```

Add the Levenshtein search extension (fuzzystrmatch) for matching money transfer tokens. Adding this to the template for
new databases allows to run tests, which create a new database every run. This must be added with superuser permissions
and per database. (see https://stackoverflow.com/a/54353033)
While at it also install Trigram Similarity (pg_trgm) which is used to quickly find tokens in the manage transactions
search.
```
$ psql -d template1 -c 'CREATE EXTENSION fuzzystrmatch;'
$ psql -d template1 -c 'CREATE EXTENSION pg_trgm;'
```

In order to run the tests grant the user the privilege to create databases by continuing as the postgresql user from above:
```
$ psql
ALTER ROLE <yourusername> CREATEDB;
```


### GnuPG

You need the GPGME Python bindings matching your gnupg version installed on your system. On Debian and Ubuntu this can be done with ```apt install python3-gpg```. For further information on installing the python bindings see https://wiki.python.org/moin/GnuPrivacyGuard#Accessing_GnuPG_via_gpgme

If your gnupg is older than 2.1.12 you need to add the following option in your ```ROOT/<GPG_HOME_FOLDER>/.gnupg/gpg-agent.conf```:

```
allow-loopback-pinentry
```

This parameter is necessary for making the password entry for encrypted keys work.

If your gnupg is older than 2.2.8 you should add the following option in your ```ROOT/<GPG_HOME_FOLDER>/.gnupg/gpg.conf```:

```
force-mdc
```

This ensures that you will only send integrity protected e-mails, no matter which key algorithm you have to use for encryption.


Import your signature key and your managing director key into this keyring as you defined in the setting variables ```GPG_HOST_USER``` and ```GPG_MANAGING_DIRECTOR```

Make sure the folder ```ROOT/<GPG_HOME_FOLDER>``` exists, where GPG_HOME_FOLDER is the name you defined in the ```ROOT/ROOT/settings_production.py```, since it is not created automatically. In this folder add a file ```gpg.conf``` stating the keyserver to use:

```
keyserver hkp://keys.gnupg.net
```

### Cron

In order for periodic tasks to function a crontab entry is needed like the following:
```
* * * * *   /path/to/installation/env/bin/python3 /path/to/installation/ROOT/manage.py scheduler
```
This will run every minute and initiate all periodic tasks.

## Run

Calling ```install_and_start_django_server.sh``` will create a virtual environment, install all necessary Python
dependencies, create a Django user, migrate all database migrations and start a webserver on 127.0.0.1:8000.

On subsequent starts only ```start_server.sh``` is needed to bring up the webserver.

## Testing Emails

For testing emails set up a local mailserver which accepts all incoming emails and displays them.

A simple built-in version in Python is the following server, which prints the received emails to the console:
```bash
$ python -m smtpd -n -c DebuggingServer localhost:1025
```

maildump offers a webinterface on port 1080, but is only available for Python 2.

```bash
$ maildump -f
```

Both servers listen on port 1025 for incoming emails.

## Credit

Thanks to the people from the Pythonfoo at Chaosdorf for building this software. Without you the office would still
struggle with outdated DOS database technology.
